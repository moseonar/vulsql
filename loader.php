<?php

ini_set('session.gc_probability', 0);
ini_set('session.gc_divisor', 1000);
ini_set('session.gc_maxlifetime', 100000);
ini_set("session.cookie_lifetime", 43200);  // 12 Stunden
ini_set("session.cache_expire", 43200);     // 12 Stunden

error_reporting(E_ALL);
ini_set('display_errors', 1);


if (! defined("BASEPATH") ) define("BASEPATH",__DIR__);

$ots="";
$ots=explode("/",$_SERVER["PHP_SELF"]);
foreach ($ots as $ot) {
    if ($ot !="") {
        $ots=$ot;
        break;
    }
}

if (! defined("BASEURL") ) define("BASEURL",$_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"]."/".$ots);;

require_once BASEPATH."/class/ticket.php";
require_once BASEPATH."/class/ticket_data.php";
require_once BASEPATH."/class/user.php";
require_once BASEPATH."/class/ticket_status.php";
require_once BASEPATH."/class/ticket_group.php";
require_once BASEPATH."/class/user_group.php";

require_once BASEPATH."/include/functions.php";
require_once BASEPATH."/include/database.php";

require_once BASEPATH."/include/auth.php";
