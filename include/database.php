<?php


$dbname="ot";
$dbuser="ot";
$dbpw="ot";
$dbprefix="";
$dbhost="localhost";


// nothing to do after this line --------------------------------------------------------------------------------

const CREATE_DEMO_DATA = false;
const DEBUG = true;
const DROP_DATABASE = false;
const ACCESS_TYPE = array("default" =>0,"user"=>10,"operator"=>20, "management"=>30, "admin"=>999);
const PRIO = array(0=>"Alle",10=>"Niedrig",20=>"Mittel", 30=>"Hoch", 40=>"Dringend", 50=>"Disaster");
const COLORS = array(0=>"default",10=>"active",20=>"info", 30=>"warning", 40=>"danger", 50=>"success");
const DISPLAY = array(0=>"bis jetzt",10=>"bis heute",20=>"Alles");
const ADD_DATA_TO_DATABASE = 0;
const UPDATE_DATA_TO_DATABASE = 1;


try {
  $_pdo = new PDO("mysql:host=".$dbhost.";dbname=".$dbname, $dbuser, $dbpw);
} catch (PDOException $pe) {
  if ($pe->getCode() == "1049") {
    //datenbank nicht vorhanden
    $_pdo = new PDO("mysql:host=".$dbhost, $dbuser, $dbpw);
    executeSQL("create database ". $dbname);
    $_pdo = new PDO("mysql:host=".$dbhost.";dbname=".$dbname, $dbuser, $dbpw);
  } else {
    echo '<br><center><h2>Please create/edit or check a database user  and/or edit the values at ./include/database.php</h2><br>Note the first user admin/admin.<br><br></center>';
    dd($pe);

    exit;
  }
} catch (Exception $e) {
  pvars ($e);
  exit;
}

if (DROP_DATABASE) dropDatabase();
setDatabaseAttributes();

if (false) {
    $tables = getArrayFromDB("show tables;");
    echo "<h3>Anzahle Tabellen: " . count($tables)."</h3><br>";
    echo '<table border=1>';
    foreach ($tables as $table => $value) {
        echo "<tr><td colspan=3><h4><br><b>".$value["Tables_in_".$dbname]."</b></h4></td></tr>";
        $rows = getArrayFromDB("describe ".$value["Tables_in_".$dbname]);
        echo "<tr>";
        echo "<td>Field</td>";
        echo "<td>Type</td>";
        echo "<td>Null</td>";
        echo "</tr>";

        foreach ($rows as $row) {
            echo "<tr>    ";
            echo "<td>".$row["Field"]."</td>";
            echo "<td>".$row["Type"]."</td>";
            echo "<td>".$row["Null"]."</td>";
            echo "</tr>    ";
        }

    }
    echo "</table>";
}





/*
    Ende database root
*/

function getArrayFromDB($sql,$db=null){
  global $_pdo;
	$localpdo=$_pdo;
	if ($db != null) $localpdo =$db;

	try {
		$stmt = $localpdo->query($sql);
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	} catch (PDOException $e) {
		echo 'DB ERROR: ' . $e->getMessage() . '<br>SQL String: <b>' . $sql."</b>";
		die();
	}
    return $results;
	}

function getRowFromDb($sql, $pdo1=null) {
	global $_pdo;
	$pdo=$_pdo;
	if ($pdo1 != null) $pdo =$pdo1;
	try {
		$stmt = $pdo->query($sql);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
	} catch (PDOException $e) {
		echo 'DB ERROR: ' . $e->getMessage() . '<br>SQL String: <b>' . $sql."</b>";
		die();
	}
  return $result;
}



function executeSQL($sql) {
	global $_pdo;
	try {
        if(is_array($sql)) {
            foreach ($sql as $sq) {
                $stmt = $_pdo->exec($sq);
        		$lid= $_pdo->lastInsertId();
            }
        } else {
            $stmt = $_pdo->exec($sql);
    		$lid= $_pdo->lastInsertId();
        }
  } catch (PDOException $pe) {
  	echo 'DB ERROR: ' . $pe->getMessage() . '<br>SQL String: <b>' . $sql."</b>";
  	exit;
  } catch (Exception $pe) {
    echo 'ERROR: ' . $pe->getMessage() . '<br>SQL String: <b>' . $sql."</b>";
    exit;
  }
  return $lid;
}


function dropDatabase(){
    global $dbprefix,$dbhost,$dbname, $dbuser, $dbpw,$_pdo;

    $sql="drop database ".$dbname."; create database ". $dbname.";";
    executeSQL($sql);
    $_pdo = new PDO("mysql:host=".$dbhost.";dbname=".$dbname, $dbuser, $dbpw);
    dd("Database dropped.",1);
}
function setDatabaseAttributes(){
    global $dbprefix,$dbhost,$dbname, $dbuser, $dbpw,$_pdo;
    $_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    executeSQL('SET NAMES \'UTF8\'');
    createTables();
}

function createSQL($table, $datas, $how, $whereKey="",$where=0){
    global $dbprefix;
    global $_pdo;
    if($how == UPDATE_DATA_TO_DATABASE) {
        $sql="update ".$dbprefix.$table . " set ";
        $z=0; $id=0;
        foreach ($datas as $data =>$key) {
            $z++;
            if ($data !="ID") {
                $sql = $sql . ' '. $data . ' = ' . $_pdo->quote($key) .' ';
                if (count($datas) > $z) $sql = $sql . ', ';
            } else {
                $id = $key;
            }
        }
        $sql = $sql . ' where '. $whereKey . ' = "' . $where .'"';
    } else {
        $sql='insert into '.$dbprefix.$table. ' ( ' ;
        $z=0; $dsql="";
        foreach ($datas as $data =>$key) {
            $z++;
            if ($data !="ID") {
                $sql=$sql . $data;
                $dsql = $dsql ." ". $_pdo->quote($key) ." ";
                if (count($datas) > $z) {
                    $sql = $sql . ', ';
                    $dsql = $dsql . ', ';
                }
            }
        }
        $sql= $sql.' ) VALUES ( ' .$dsql . ')';
    }
    return $sql;
}



function createTables(){
  global $dbprefix;


  if (is_bool(getRowFromDb("SHOW TABLES LIKE '".$dbprefix."user_group'"))){
    $sql=array();
    $sql[]='
      DROP TABLE IF EXISTS `'.$dbprefix.'user_group`;
      CREATE TABLE `'.$dbprefix.'user_group` (
      `ID` int(11) NOT NULL AUTO_INCREMENT,
      `ACCESS` int(11) default '.ACCESS_TYPE["default"].',
      `NAME` varchar(255) NOT NULL,
      `DESCRIPTION` varchar(255) default "",
      `EMAIL` varchar(255) default "",
      `DATE_CREATE` datetime DEFAULT NOW(),
       PRIMARY KEY (`ID`)
      ) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
     ';
     executeSQL($sql);
     $user_group[0] = executeSQL('insert into '. $dbprefix.'user_group (ACCESS, NAME) values ('. ACCESS_TYPE["default"] . ', "default")');
     $user_group[1] = executeSQL('insert into '. $dbprefix.'user_group (ACCESS, NAME) values ('. ACCESS_TYPE["user"] . ', "user")');
     $user_group[2] = executeSQL('insert into '. $dbprefix.'user_group (ACCESS, NAME) values ('. ACCESS_TYPE["operator"] . ', "operator")');
     $user_group[3] = executeSQL('insert into '. $dbprefix.'user_group (ACCESS, NAME) values ('. ACCESS_TYPE["management"] . ', "management")');
     $user_group[4] = executeSQL('insert into '. $dbprefix.'user_group (ACCESS, NAME) values ('. ACCESS_TYPE["admin"] . ', "admin")');
  }


    if (is_bool(getRowFromDb("SHOW TABLES LIKE '".$dbprefix."ticket_group'"))){
    $sql=array();
    $sql[]='
      DROP TABLE IF EXISTS `'.$dbprefix.'ticket_group`;
      CREATE TABLE `'.$dbprefix.'ticket_group` (
      `ID` int(11) NOT NULL AUTO_INCREMENT,
      `ACCESS` int(11) default 0,
      `NAME` varchar(255) NOT NULL,
      `DESCRIPTION` varchar(255) default "",
      `EMAIL` varchar(255) default "",
      `DATE_CREATE` datetime DEFAULT NOW(),
       PRIMARY KEY (`ID`)
      ) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
     ';
     executeSQL($sql);
     //const ACCESS_TYPE = array("default" =>0,"user"=>10,"operator"=>20, "management"=>30, "admin"=>40);
     $ticket_group[0] = executeSQL('insert into '. $dbprefix.'ticket_group (ACCESS, NAME) values ('. ACCESS_TYPE["default"] . ', "Ungruppiert")');
     $ticket_group[1] = executeSQL('insert into '. $dbprefix.'ticket_group (ACCESS, NAME) values ('. ACCESS_TYPE["user"] . ', "Veraltung")');
     $ticket_group[2] = executeSQL('insert into '. $dbprefix.'ticket_group (ACCESS, NAME) values ('. ACCESS_TYPE["user"] . ', "IT")');
     $ticket_group[3] = executeSQL('insert into '. $dbprefix.'ticket_group (ACCESS, NAME) values ('. ACCESS_TYPE["user"] . ', "Support")');
     $ticket_group[4] = executeSQL('insert into '. $dbprefix.'ticket_group (ACCESS, NAME) values ('. ACCESS_TYPE["user"] . ', "Entwicklung")');
     $ticket_group[5] = executeSQL('insert into '. $dbprefix.'ticket_group (ACCESS, NAME) values ('. ACCESS_TYPE["management"] . ', "Geschäftsleitung")');

  }


  $sql=array();
  if ( is_bool(getRowFromDb("SHOW TABLES LIKE '".$dbprefix."user'")) ){
      $sql=array();
      $sql[]='
      DROP TABLE IF EXISTS `'.$dbprefix.'user`;
      CREATE TABLE `'.$dbprefix.'user` (
      `ID` int(11) NOT NULL AUTO_INCREMENT,
      `USER_GROUP_ID` int(11) NOT NULL ,
      `TICKET_GROUP_ID` int(11) NOT NULL ,
      `LOGIN` varchar(40) NOT NULL,
      `PASSWORD` varchar(128) NOT NULL,
      `FULL_NAME` varchar(255) default "",
      `EMAIL` varchar(255) NOT NULL,
      `SORT` int(11) DEFAULT 100,
      `DATE_CREATE` datetime DEFAULT NOW(),
       PRIMARY KEY (`ID`)
      ) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
     ';
     executeSQL($sql);
     $userid = executeSQL("insert into ". $dbprefix."user (SORT, TICKET_GROUP_ID, USER_GROUP_ID, LOGIN,PASSWORD, FULL_NAME,EMAIL) values ('10','" .ACCESS_TYPE["management"] . "','".$user_group[4]."','admin', MD5('admin'), 'Admin User' ,'support@domain' )");
     if (CREATE_DEMO_DATA) $userid2 = executeSQL("insert into ". $dbprefix."user (TICKET_GROUP_ID, USER_GROUP_ID, LOGIN,PASSWORD, FULL_NAME,EMAIL) values ('" .ACCESS_TYPE["user"] . "','".$user_group[1]."','test', MD5('test'), 'Test User' ,'Test@domain.de' )");
  }


  if (is_bool(getRowFromDb("SHOW TABLES LIKE '".$dbprefix."ticket_status'"))){
    $sql=array();
    $sql[]='
      DROP TABLE IF EXISTS `'.$dbprefix.'ticket_status`;
      CREATE TABLE `'.$dbprefix.'ticket_status` (
      `ID` int(11) NOT NULL AUTO_INCREMENT,
      `ACCESS` int(11) default 0,
      `NAME` varchar(255) NOT NULL,
      `SORT` int(11) default 0,
      `CLOSED` boolean default false,
      `COLOR` varchar(255) default "default",
      `DESCRIPTION` varchar(255) default "",
      `DATE_CREATE` datetime DEFAULT NOW(),
       PRIMARY KEY (`ID`)
      ) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
     ';
     executeSQL($sql);
     $ticket_status[0] = executeSQL('insert into '. $dbprefix.'ticket_status (ACCESS, NAME, SORT) values ('. ACCESS_TYPE["default"] . ', "Kein Status", "100")');
     $ticket_status[1] = executeSQL('insert into '. $dbprefix.'ticket_status (ACCESS, NAME, SORT) values ('. ACCESS_TYPE["default"] . ', "Offen", "200")');
     $ticket_status[2] = executeSQL('insert into '. $dbprefix.'ticket_status (ACCESS, NAME, SORT) values ('. ACCESS_TYPE["default"] . ', "Wartet", "300")');
     $ticket_status[3] = executeSQL('insert into '. $dbprefix.'ticket_status (ACCESS, NAME, SORT) values ('. ACCESS_TYPE["default"] . ', "In Arbeit", "400")');
     $ticket_status[4] = executeSQL('insert into '. $dbprefix.'ticket_status (ACCESS, NAME, SORT,CLOSED) values ('. ACCESS_TYPE["default"] . ', "Erledigt", "500", "1" )');
     $ticket_status[5] = executeSQL('insert into '. $dbprefix.'ticket_status (ACCESS, NAME, SORT,CLOSED) values ('. ACCESS_TYPE["default"] . ', "Gelöscht", "999", "1")');

  }



  if (is_bool(getRowFromDb("SHOW TABLES LIKE '".$dbprefix."ticket'"))){
    $sql=array();
    $sql[]='
      DROP TABLE IF EXISTS `'.$dbprefix.'ticket`;
      CREATE TABLE `'.$dbprefix.'ticket` (
      `ID` int(11) NOT NULL AUTO_INCREMENT,
      `TICKET_STATUS_ID` int(11) NOT NULL,
      `TICKET_GROUP_ID` int(11) NOT NULL,
      `USER_ID` int(11) NOT NULL,
      `USER_CREATE_ID` int(11) NOT NULL,
      `NAME` varchar(255) NOT NULL,
      `DATE_READY` datetime  DEFAULT NOW(),
      `REMIND` datetime  DEFAULT NOW(),
      `ACCESS` int(11) default 0,
      `PRIO` int(11) default 0,
      `SEEN` int(11) default 0,
      `PARENT` int(11) default 0,
      `DATE_CREATE` datetime DEFAULT NOW(),
      `DATE_MODIFY` datetime DEFAULT NOW(),
       PRIMARY KEY (`ID`)
      ) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
     ';
     executeSQL($sql);
     if (CREATE_DEMO_DATA) $t[1] = executeSQL('insert into '. $dbprefix.'ticket (ACCESS, TICKET_STATUS_ID, TICKET_GROUP_ID, USER_ID,  NAME, USER_CREATE_ID) values ("'.ACCESS_TYPE["admin"].'","'.$ticket_status[1].'", "'.$ticket_group[1].'", "'.$userid.'", "Testticket 10","'.$userid.'")');
     if (CREATE_DEMO_DATA) $t[2] = executeSQL('insert into '. $dbprefix.'ticket (ACCESS, TICKET_STATUS_ID, TICKET_GROUP_ID, USER_ID,  NAME, USER_CREATE_ID) values ("'.ACCESS_TYPE["admin"].'","'.$ticket_status[0].'", "'.$ticket_group[0].'", "'.$userid.'", "Testticket 05","'.$userid.'")');
     if (CREATE_DEMO_DATA) $t[3] = executeSQL('insert into '. $dbprefix.'ticket (ACCESS, TICKET_STATUS_ID, TICKET_GROUP_ID, USER_ID,  NAME, USER_CREATE_ID) values ("'.ACCESS_TYPE["admin"].'","'.$ticket_status[2].'", "'.$ticket_group[2].'", "'.$userid.'", "Testticket 20","'.$userid.'")');
  }

  if (is_bool(getRowFromDb("SHOW TABLES LIKE '".$dbprefix."ticket_data'"))){
    $sql='
      DROP TABLE IF EXISTS `'.$dbprefix.'ticket_data`;
      CREATE TABLE `'.$dbprefix.'ticket_data` (
      `ID` int(11) NOT NULL AUTO_INCREMENT,
      `TICKET_ID` int(11) NOT NULL,
      `USER_ID` int(11) NOT NULL,
      `SUBJECT` varchar(255) default "",
      `PROCESS` TEXT,
      `LOG` TEXT,
      `MINUTES` int(11) default 0,
      `DATE_CREATE` datetime DEFAULT NOW(),
       PRIMARY KEY (`ID`)
      ) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
     ';
     executeSQL($sql);
     if (CREATE_DEMO_DATA) executeSQL('insert into '. $dbprefix.'ticket_data (USER_ID, TICKET_ID, SUBJECT, PROCESS, MINUTES) values ("'.$userid2.'", "'.$t[1].'", "Test name 1", "RANDOM STRING: '.generateRandomString(500).'", "'.rand(1,300).'")');
     if (CREATE_DEMO_DATA) executeSQL('insert into '. $dbprefix.'ticket_data (USER_ID, TICKET_ID, SUBJECT, PROCESS, MINUTES) values ("'.$userid.'", "'.$t[2].'", "Test name 2", "RANDOM STRING: '.generateRandomString(500).'", "'.rand(1,300).'")');
     if (CREATE_DEMO_DATA) executeSQL('insert into '. $dbprefix.'ticket_data (USER_ID, TICKET_ID, SUBJECT, PROCESS, MINUTES) values ("'.$userid2.'", "'.$t[3].'", "Test name 3", "RANDOM STRING: '.generateRandomString(500).'", "'.rand(1,300).'")');
     if (CREATE_DEMO_DATA) executeSQL('insert into '. $dbprefix.'ticket_data (USER_ID, TICKET_ID, SUBJECT, PROCESS, MINUTES) values ("'.$userid.'", "'.$t[1].'", "Test zu 1, 2. Eintrag", "RANDOM STRING: '.generateRandomString(500).'", "'.rand(1,300).'")');
  }



}
