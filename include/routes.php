<?php


$route=array_merge($_POST, $_GET);

if (isset($route["_FORM"])) {
    if ($route["_FORM"] == "ticket" && isset($route["BUTTON_SAVE"])) {
        include "ui/menu.php";
        (new Ticket())->saveTicket($route);
        exit;
    }


    if ($route["_FORM"] == "home") {
        if (isset($route["HOME"]) or isset($route["BUTTON_FILTER_RESET"])){
            header('Location: index.php');
            exit;
        }

        if (isset($route["ADD_TICKET"])){
            $ticket = (new Ticket())->newTicket($route);
            require "ui/ticket.php";
            exit;
        }
        (new Ticket())->startGui($route);
        exit;
    }
}


if (isset($route["showTicketId"])) {
  $ticket = (new Ticket())->getTicketForTicketId($route["showTicketId"]);
  if(	$ticket["USER_ID"] == $_SESSION['ot']["user"]["ID"] )  (new Ticket())->setSeen($route["showTicketId"]);
  require "ui/ticket.php";
  exit;
}



if (isset($route["SET_MASTER_TICKET_FOR"]) and  $route["SET_MASTER_TICKET_FOR"] >0 ) {
    $_SESSION["ot"]["SET_MASTER_TICKET_FOR"]=$route["SET_MASTER_TICKET_FOR"];
}

if (isset($route["ADD_SUBTICKET_FOR_MASTER_TICKET"]) and $route["ADD_SUBTICKET_FOR_MASTER_TICKET"]>0) {
      $_SESSION["ot"]["SET_MASTER_TICKET_FOR"]=$route["ADD_SUBTICKET_FOR_MASTER_TICKET"];
      $ticket = (new Ticket())->newTicket($route);
      require "ui/ticket.php";
      exit;
}

if (isset($route["BREAK_SUBTICKET_FOR_MASTER_TICKET"])) {
  $_SESSION["ot"]["SET_MASTER_TICKET_FOR"] = 0;
  unset( $_SESSION["ot"]["SET_MASTER_TICKET_FOR"]);
}

if (isset($route["BREAK_SET_MASTER_TICKET_FOR"])    or  isset($route["ADD_MASTER_TICKET_FOR"]) ) {
  if (isset($route["ADD_MASTER_TICKET_FOR"])    and  $route["ADD_MASTER_TICKET_FOR"]>0 ) {
    (new Ticket())->setParentForId($_SESSION["ot"]["SET_MASTER_TICKET_FOR"], $route["ADD_MASTER_TICKET_FOR"]);
  }
  $_SESSION["ot"]["SET_MASTER_TICKET_FOR"] = 0;
  unset( $_SESSION["ot"]["SET_MASTER_TICKET_FOR"]);
}


// USER ------------------------------------------------------------------------------------------------------------
if (isset($route["tree"])) {
    include "ui/menu.php";
    include "ui/tree_view.php";
    exit;
}


// USER ------------------------------------------------------------------------------------------------------------
if (isset($route["user"])) {
    $users=(new User())->getAllUsers(true);
    include "ui/users.php";
    exit;
}
if (isset($route["editUserId"]))  {
    $user=(new User())->getUsersForId($route["editUserId"]);
    include "ui/user.php";
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="user" && isset($route["BUTTON_SAVE"])) {
    if (isset($route["BUTTON_SAVE"])) {
        (new User())->saveUser($route);
    }
    header("Location: index.php?user");
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="users" && isset($route["ADD_USER"])) {
    $user=(new User())->getNewUsers();
    include "ui/user.php";
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="user" && isset($route["BUTTON_DELETE"])) {
    (new User())->delete($route["ID"]);
    header("Location: index.php?user");
    exit;
}
// USER ------------------------------------------------------------------------------------------------------------


// USER GROUPS  -----------------------------------------------------------------------------------------------------
if (isset($route["userGroups"])) {
    $userGroups=(new UserGroup())->getAllUsersGroups(true);
    include "ui/user_groups.php";
    exit;
}
if (isset($route["editUserGroupId"]))  {
    $userGroup=(new UserGroup())->getUsersGroupForId($route["editUserGroupId"]);
    include "ui/user_group.php";
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="userGroup" && isset($route["BUTTON_SAVE"])) {
    (new UserGroup())->saveUserGroup($route);
    header("Location: index.php?userGroups");
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="userGroups" && isset($route["ADD_USER_GROUP"])) {
    $userGroup=(new UserGroup())->getNewUserGroup();
    include "ui/user_group.php";
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="userGroup" && isset($route["BUTTON_DELETE"])) {
    (new UserGroup())->delete($route["ID"]);
    header("Location: index.php?userGroups");
    exit;
}
// USER GROUPS  -----------------------------------------------------------------------------------------------------


// TICKET STATUS  ---------------------------------------------------------------------------------------------------
if (isset($route["ticketStati"])) {
    $ticketStati=(new TicketStatus())->getAllTicketStatus();
    include "ui/ticket_stati.php";
    exit;
}
if (isset($route["editTicketStatusId"]))  {
    $ticketStatus=(new TicketStatus())->getTicketStatusForId($route["editTicketStatusId"]);
    include "ui/ticket_status.php";
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="ticketStatus" && isset($route["BUTTON_SAVE"])) {
    (new TicketStatus())->save($route);
    header("Location: index.php?ticketStati");
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="ticketStati" && isset($route["ADD_TICKET_STATUS"])) {
    $ticketStatus=(new TicketStatus())->getNewTicketStatus();
    include "ui/ticket_status.php";
    exit;
}
if (isset($route["_FORM"]) && $route["_FORM"]=="ticketStatus" && isset($route["BUTTON_DELETE"])) {
    (new TicketStatus())->delete($route["ID"]);
    header("Location: index.php?ticketStati");
    exit;
}

// TICKET STATUS  ---------------------------------------------------------------------------------------------------


if (isset($route["resetDatabase"])) {
    include "ui/menu.php";
    //dropDatabase();
    //setDatabaseAttributes();
    exit;
}

if (isset($route["logout"])) {
    session_start();
    session_destroy();
    header('Location: index.php');
    exit;
}









//letzte Zeile
header("Location: index.php");
