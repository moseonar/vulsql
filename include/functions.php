<?php

function dd($m, $mdie=0){
	echo '<br><center><table border=1><tr>
		<td colspan="3" rowspan="1" style="vertical-align: top; background-color: rgb(200, 200, 200);">';
	echo "<br><b>DEBUG Ausgabe";
	echo "<br><hr></td></tr>";

	if(is_object($m)){
		$m = dismount($m);
		// nur die Variablen anzeigen, welche getter und setter haben
		//$m = entity2array($m);
	}

	if(! is_array($m)){
		if (is_bool($m)) $m = ($m?"(boolean) True":"(boolean) False");
		echo "<tr><td>Einzelne Variable</td><td>". $m ."</td></tr>";
	} else {
		echo "<tr><td><b>Nr.</b></td><td><b>Key</b></td><td><b>Value</b></td></tr>";
		printsubarray($m);
	}
	echo "</table></center>";
	if ($mdie) {
		echo "<br><br>PROGRAMMENDE.";
		die();
	}
}
function printsubarray($a){
	$i=-1;
	foreach ($a as $k => $value) {
		$i++;
		echo "<tr>";
		echo "<td>" . $i ."</td><td>". $k . "</td><td>";
		if (is_object($value)) {
			$value = dismount($value);
		}
		if (is_array($value)) {
			printsubarray($value);
			echo "<tr><td></td>";
			echo "<td colspan=2><hr></td></tr>";
		} else {
			echo  $value . "</td>";
		}
		echo "</tr>";
	}
}
function dismount($object) {
	$reflectionClass = new ReflectionClass(get_class($object));
	$name = get_class ($object);
	$name = str_replace('\\', "\\\\", $name);
	$array = array();
	foreach ($reflectionClass->getProperties() as $property) {
		$property->setAccessible(true);
		//$array[$name.'["'.$property->getName().'"]'] = $property->getValue($object);
		$array[$property->getName()] = $property->getValue($object);
		$property->setAccessible(false);
	}
	return $array;
}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getMysqldateFromInput($localDate){
	//Format muss "dd.mm.yy hh:mm" sein
	if (is_bool($localDate) || trim($localDate)== "") $localDate = date("d.m.y H:i");
	$a = DateTime::createFromFormat("d.m.y H:i:s", trim($localDate).":00");
	return $a->format('Y-m-d H:i:s');
}
function getDateFromMysql($mysqlDate){
	$phpdate = strtotime( $mysqlDate );
	$localdate = date( 'd.m.y H:i', $phpdate );
	return $localdate;
}
function mytoday($mysql=true){
	if ($mysql){
		return  trim(date("y-m-d H:i:s"));
	} else {
		return trim(date("d.m.y H:i:s"));
	}
}
