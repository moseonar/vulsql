<?php
    include "menu.php";

    $info=($ticketStatus["ID"] == 0 ? "Neuen Ticket-Status anlegen": "Ticket-Status '". $ticketStatus["NAME"]. "' editieren");

    $ticketStati=(new TicketStatus())->getAllTicketStatus();
    $showDelete = (count($ticketStati)<2 || $ticketStatus["SORT"] == 999 ? false :true);
    if ($ticketStatus["ID"]==0) $showDelete=false;


?>
<div class="container">
    <form class="form-horizontal" role="form" id="user" name="userGroup" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="ticketStatus">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <h3><?php echo $info; ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="ID" class="col-md-2 control-label">ID</label>
                            <div class="col-md-3">
                              <input readonly="readonly" id="ID" type="text" class="form-control" name="ID" value="<?php echo $ticketStatus["ID"]; ?>">
                            </div>
                            <label for="DATE_CREATE" class="col-md-2 control-label col-md-offset-2">Erstellt</label>
                            <div class="col-md-3" >
                              <input disabled id="DATE_CREATE" type="text" class="form-control text-center" name="DATE_CREATE" value="<?php echo getDateFromMysql($ticketStatus["DATE_CREATE"]) ?>">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="NAME" class="col-md-2 control-label">Name</label>
                            <div class="col-md-3">
                              <input <?php echo ($ticketStatus["SORT"] == 999?'readonly="readonly"':""); ?> autocomplete="off" id="NAME" type="text" class="form-control" name="NAME" value="<?php echo $ticketStatus["NAME"]; ?>" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ACCESS" class="col-md-2 control-label">Access</label>
                            <div class="col-md-3">
                              <input <?php echo ($ticketStatus["SORT"] == 999?'readonly="readonly"':""); ?> type="number" min="0" max="998" title="Format: 3 digits" id="ACCESS" class="form-control " name="ACCESS" value="<?php echo $ticketStatus["ACCESS"];?>" required >
                            </div>
                            <div class="col-md-3">
                                (default Access = 0)
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="SORT" class="col-md-2 control-label">Sort</label>
                            <div class="col-md-3">
                              <input <?php echo ($ticketStatus["SORT"] == 999?'readonly="readonly"':""); ?> type="number" min="0" max="998" title="Format: 3 digits" id="SORT" class="form-control " name="SORT" value="<?php echo $ticketStatus["SORT"];?>" required >
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="TICKET_GROUP_ID" class="col-md-2 control-label">Bootstrap Color</label>
                            <div class="col-md-3">
                                <select class="form-control" name="COLOR">
                                <?php
                                    foreach (COLORS as $color =>$key) {
                                        $selected = ($ticketStatus["COLOR"] == $color?"selected":"");
                                        echo '
                                            <option '.$selected.' value="'.$color.'">'.$key.'</option>
                                        ';
                                    }
                                ?>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="CLOSED" class="col-md-2 control-label">Geschlossen</label>
                            <div class="col-md-6">
                                <div class="checkbox">
                                  <label><input <?php echo ($ticketStatus["SORT"] == 999 ? 'disabled':""); ?> name="CLOSED" <?php echo ($ticketStatus["CLOSED"]== 1 ?"checked":""); ?> type="checkbox" value="">(wird nicht in der Ticket ansicht gezeigt)</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">

                            <label for="DESCRIPTION" class="col-md-2 control-label">Beschreibung</label>
                            <div class="col-md-10">
                              <input   autocomplete="off" id="DESCRIPTION" type="text" class="form-control" name="DESCRIPTION" value="<?php echo $ticketStatus["DESCRIPTION"]; ?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 pull-right">
                                <?php
                                    $buttons[2]["show"] = true;
                                    $buttons[1]["show"] = $showDelete;
                                    $buttons[1]["class"] = "confirm btn btn-warning";
                                    $buttons[1]["link"] = "href='#'";
                                    $buttons[0]["link"] = "href='index.php?ticketStati'";
                                    include "buttons.php";
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" charset="utf-8">
    $(function() {
        $('.confirm').click(function() {
            return window.confirm("Diesen Ticketstatus wirklich entfernen?\n(Zugewiesen Tickets verbleiben im System)");
        });
    });
</script>
