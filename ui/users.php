<?php
    include "menu.php";

    $ShowNewuserButton=($_SESSION['ot']["user"]["ACCESS"] == ACCESS_TYPE["admin"]?true:false);
    
?>
<div class="container">
    <form class="form-horizontal" role="form" id="users" name="home" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="users">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-5">
                                <h3>Users</h3>
                            </div>
                            <div class="col-md-3 pull-right">
                                <?php
                                    if ($ShowNewuserButton){
                                        echo '
                                            <button  class="btn btn-primary" type="submit" name="ADD_USER">
                                                <i class="glyphicon glyphicon-plus"></i> Neuen User hinzuf&uuml;gen
                                            </button>
                                        ';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <table class="table table-hover" id="tickets">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Login Name</th>
                                        <th>Full Name</th>
                                        <th>E-Mail</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                        foreach ($users as $user) {
                                            echo '
                                            <tr class="clickable-row" data-href="index.php?editUserId='.$user["ID"].'" >
                                            <td>'.$user["ID"].'</td>
                                            <td>'.$user["LOGIN"].'</td>
                                            <td>'. $user["FULL_NAME"].'</td>
                                            <td>'. $user["EMAIL"].'</td>
                                            </tr>';
                                        }
                                       ?>
                                   </tbody>
                               </table>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $.fn.dataTable.moment( 'DD.MM.YY HH:mm' );
         $('#tickets').DataTable({
              "order": [[ 1, "desc" ]],
             "language": {
                   "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/German.json"
              }
         });

    } );

     jQuery(document).ready(function($) {
         $(".clickable-row").click(function() {
             window.location = $(this).data("href");
         });
     });
</script>
