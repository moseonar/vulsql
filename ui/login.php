<br>
<br>
<div class="container">
    <form class="form-horizontal" role="form" id="login" name="login" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="login">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading ">
                        <div class="form-group text-center">
                            <h4>Login Page</h4>
                        </div>
                        <div class="form-group">
                            <label for="LOGIN" class="col-md-4 control-label">Login Name</label>
                            <div class="col-md-7">
                                <input  autocomplete="off" id="LOGIN" type="text" class="form-control " name="LOGIN" value=""   required  autofocus  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="PASSWORD" class="col-md-4 control-label">Passwort</label>
                            <div class="col-md-7">
                                <input  autocomplete="off" id="PASSWORD" type="password" class="form-control " name="PASSWORD" value=""   required  >
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-7 col-md-offset-4">
                                <button class="btn btn-primary" type="submit" name="LOG_IN">
                                    <i class="glyphicon glyphicon-log-in"></i> Login
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
