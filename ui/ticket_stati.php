<?php
    include "menu.php";

//$ticketStati
?>
<div class="container">
    <form class="form-horizontal" role="form" id="ticket_stati" name="home" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="ticketStati">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-5">
                                <h3>Ticket Status</h3>
                            </div>
                            <div class="col-md-4 pull-right">
                                <?php
                                    echo '
                                        <button  class="btn btn-primary" type="submit" name="ADD_TICKET_STATUS">
                                            <i class="glyphicon glyphicon-plus"></i> Neuen Ticket Status hinzuf&uuml;gen
                                        </button>
                                    ';
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <table class="table table-hover" id="tickets">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Sort</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                        foreach ($ticketStati as $ticketStatus) {
                                            echo '
                                            <tr class="clickable-row" data-href="index.php?editTicketStatusId='.$ticketStatus["ID"].'" >
                                            <td>'.$ticketStatus["ID"].'</td>
                                            <td>'.$ticketStatus["NAME"].'</td>
                                            <td>'. $ticketStatus["SORT"].'</td>
                                            </tr>';
                                        }
                                       ?>
                                   </tbody>
                               </table>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $.fn.dataTable.moment( 'DD.MM.YY HH:mm' );
         $('#tickets').DataTable({
              "order": [[ 2, "asc" ]],
             "language": {
                   "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/German.json"
              }
         });

    } );

     jQuery(document).ready(function($) {
         $(".clickable-row").click(function() {
             window.location = $(this).data("href");
         });
     });
</script>
