<?php
    include "menu.php";
    $info=($userGroup["ID"] == 0 ? "Neue User-Gruppe anlegen": "User-Gruppe '". $userGroup["NAME"]. "' editieren");

    $userGroups=(new UserGroup())->getAllUsersGroups();
    $showDelete = (count($userGroups) <2 ? false :true);
    if ($userGroup["ID"]==0) $showDelete=false;
    if ($userGroup["ACCESS"]==999    ) $showDelete=false;


?>
<div class="container">
    <form class="form-horizontal" role="form" id="user" name="userGroup" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="userGroup">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <h3><?php echo $info; ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="ID" class="col-md-2 control-label">ID</label>
                            <div class="col-md-3">
                              <input readonly="readonly" id="ID" type="text" class="form-control" name="ID" value="<?php echo $userGroup["ID"]; ?>">
                            </div>
                            <label for="DATE_CREATE" class="col-md-2 control-label col-md-offset-2">Erstellt</label>
                            <div class="col-md-3" >
                              <input disabled id="DATE_CREATE" type="text" class="form-control text-center" name="DATE_CREATE" value="<?php echo getDateFromMysql($userGroup["DATE_CREATE"]) ?>">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="NAME" class="col-md-2 control-label">Name</label>
                            <div class="col-md-3">
                              <input <?php echo ($userGroup["ACCESS"]==999?'readonly="readonly"':"") ; ?> autocomplete="off" id="NAME" type="text" class="form-control" name="NAME" value="<?php echo $userGroup["NAME"]; ?>" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="SORT" class="col-md-2 control-label">Access</label>
                            <div class="col-md-3">
                              <input <?php echo ($userGroup["ACCESS"]==999?'readonly="readonly"':"") ; ?> type="number" min="1" max="998" title="Format: 3 digits" id="ACCESS" class="form-control " name="ACCESS" value="<?php echo $userGroup["ACCESS"];?>" required >
                            </div>
                            <div class="col-md-3">
                                (default User Access = <?php echo ACCESS_TYPE["user"]; ?>)
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="EMAIL" class="col-md-2 control-label">E-Mail</label>
                            <div class="col-md-3">
                              <input autocomplete="off" id="EMAIL" type="email" class="form-control" name="EMAIL" value="<?php echo $userGroup["EMAIL"]; ?>" >
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="DESCRIPTION" class="col-md-2 control-label">Beschreibung</label>
                            <div class="col-md-10">
                              <input autocomplete="off" id="DESCRIPTION" type="text" class="form-control" name="DESCRIPTION" value="<?php echo $userGroup["DESCRIPTION"]; ?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 pull-right">
                                <?php
                                    $buttons[2]["show"] = true;
                                    $buttons[1]["show"] = $showDelete;
                                    $buttons[1]["class"] = "confirm btn btn-warning";
                                    $buttons[1]["link"] = "href='#'";
                                    $buttons[0]["link"] = "href='index.php?userGroups'";
                                    include "buttons.php";
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" charset="utf-8">
    $(function() {
        $('.confirm').click(function() {
            return window.confirm("Diese Usergruppe wirklich entfernen?\n(Zugewiesen Tickets/User verbleiben im System)");
        });
    });
</script>
