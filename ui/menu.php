

<?php include BASEPATH."/ui/header.php"; ?>

<br>
<nav class="navbar navbar-default ">
    <div class="container">
       <div class="row">
           <div class="col-md-12 col-md-offset-0">
                   <div class="container-fluid">
                   <div class="navbar-header">
                           <a class="navbar-brand" href=#">ownTicketSystem</a>
                   </div>
                   <ul class="nav navbar-nav">
                       <li><a href="index.php">Home</a></li>

                       <li class="dropdown">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">Konfiguration
                             <span class="caret"></span></a>
                         <ul class="dropdown-menu">
                           <li><a href="index.php?user">Users</a></li>
                           <li><a href="index.php?userGroups">Users Groups</a></li>
                           <li><a href="index.php?ticketStati">Ticket Status</a></li>

                           <li class="divider"></li>
                           <li class="nav-link disabled"><a href="#">Ticket Group</a></li>
                         </ul>
                       </li>
                   </ul>
                   <ul class="nav navbar-nav navbar-right">
                       <li>
                            <a href="index.php?logout">
                                <span class="glyphicon glyphicon-log-out"></span> <?php echo $_SESSION['ot']["user"]["FULL_NAME"]; ?>
                            </a>
                            <form id="logout-form" action="logout" method="POST" style="display: none;">
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
