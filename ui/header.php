<!DOCTYPE html>
    <html lang=en>
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ownTicketSystem</title>
    </head>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>/vendor/jquery/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>/vendor/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASEURL; ?>/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">


    <!-- JS REIHENFOLGEN NICHT AENDERN -->
    <script src="<?php echo BASEURL; ?>/vendor/jquery/jquery.js"></script>
    <script src="<?php echo BASEURL; ?>/vendor/bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo BASEURL; ?>/vendor/jquery/datatables.min.js"></script>
    <script src="<?php echo BASEURL; ?>/vendor/jquery/jquery-ui.js"></script>

    <script src="<?php echo BASEURL; ?>/vendor/jquery/moment.min.js"></script>
    <script src="<?php echo BASEURL; ?>/vendor/jquery/datetime-moment.js"></script>

    <script src="<?php echo BASEURL; ?>/vendor/bootstrap-datetimepicker/js/moment-with-locales.js"></script>
    <script src="<?php echo BASEURL; ?>/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

    <script src="<?php echo BASEURL; ?>/vendor/bootstrap-treeview/bootstrap-treeview.min.js"></script>
