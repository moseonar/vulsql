<!--
    vorher definierte Variabalen für 3 Buttons:

    buttons[]["enabled"] = boolean / default false
    buttons[]["show"] = boolean / default false
    buttons[]["caption"] = string / default "Zurück / Speichern"

-->
<?php
    if (! isset($buttons)) $buttons = array();
    for ($i=0; $i < 3; $i++) {
        if (! isset($buttons[$i])) $buttons[$i] = array();
        if (! isset($buttons[$i]["enabled"])) $buttons[$i]["enabled"] = true;
    }
    if (! isset($buttons[0]["caption"])) $buttons[0]["caption"] = "Zur&uuml;ck";
    if (! isset($buttons[0]["icon"])) $buttons[0]["icon"] = "glyphicon glyphicon-new-window";
    if (! isset($buttons[0]["class"])) $buttons[0]["class"] = "btn btn-info";
    if (! isset($buttons[0]["show"])) $buttons[0]["show"] = true;
    if (! isset($buttons[0]["typ"])) $buttons[0]["typ"] = "a";
    if (! isset($buttons[0]["name"])) $buttons[0]["name"] = "";
    if (! isset($buttons[0]["link"])) $buttons[0]["link"] = "href='index.php?BREAK_SUBTICKET_FOR_MASTER_TICKET'";

//<a class="btn btn-info" href="{{ url('config/taxes') }}">

    if (! isset($buttons[1]["caption"])) $buttons[1]["caption"] = "L&ouml;schen";
    if (! isset($buttons[1]["icon"])) $buttons[1]["icon"] = "glyphicon glyphicon-remove";
    if (! isset($buttons[1]["class"])) $buttons[1]["class"] = "btn btn-warning";
    if (! isset($buttons[1]["show"])) $buttons[1]["show"] = false;
    if (! isset($buttons[1]["typ"])) $buttons[1]["typ"] = "button";
    if (! isset($buttons[1]["link"])) $buttons[1]["link"] = "";
    if (! isset($buttons[1]["name"])) $buttons[1]["name"] = "BUTTON_DELETE";

    if (! isset($buttons[2]["caption"])) $buttons[2]["caption"] = "Speichern";
    if (! isset($buttons[2]["icon"])) $buttons[2]["icon"] = "glyphicon glyphicon-floppy-disk";
    if (! isset($buttons[2]["class"])) $buttons[2]["class"] = "btn btn-primary";
    if (! isset($buttons[2]["show"])) $buttons[2]["show"] = false;
    if (! isset($buttons[2]["typ"])) $buttons[2]["typ"] = "button";
    if (! isset($buttons[2]["link"])) $buttons[2]["link"] = "";
    if (! isset($buttons[2]["name"])) $buttons[2]["name"] = "BUTTON_SAVE";
?>

<div class="col-md-12 well">
    <div class=" text-center ">
        <?php
            foreach ($buttons as $button) {
                if ($button["show"]) {
                    echo '
                        <'.$button["typ"].' '.$button["link"].' class="'.$button["class"].'" type="submit" name="'.$button["name"].'">
                            '.$button["caption"].' <i class="'.$button["icon"].'"></i>
                        </'.$button["typ"].'>
                    ';
                }
            }
         ?>
    </div>
</div>
