<?php
    include "menu.php";

    $ShowNewuserButton=($_SESSION['ot']["user"]["ACCESS"] == ACCESS_TYPE["admin"]?true:false);

?>
<div class="container">
    <form class="form-horizontal" role="form" id="user_groups" name="home" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="userGroups">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-5">
                                <h3>User Groups</h3>
                            </div>
                            <div class="col-md-3 pull-right">
                                <?php
                                    if ($ShowNewuserButton){
                                        echo '
                                            <button  class="btn btn-primary" type="submit" name="ADD_USER_GROUP">
                                                <i class="glyphicon glyphicon-plus"></i> Neuen Gruppe hinzuf&uuml;gen
                                            </button>
                                        ';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <table class="table table-hover" id="tickets">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Access</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                        foreach ($userGroups as $userGroup) {
                                            echo '
                                            <tr class="clickable-row" data-href="index.php?editUserGroupId='.$userGroup["ID"].'" >
                                            <td>'.$userGroup["ID"].'</td>
                                            <td>'.$userGroup["NAME"].'</td>
                                            <td>'. $userGroup["ACCESS"].'</td>
                                            </tr>';
                                        }
                                       ?>
                                   </tbody>
                               </table>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $.fn.dataTable.moment( 'DD.MM.YY HH:mm' );
         $('#tickets').DataTable({
              "order": [[ 2, "asc" ]],
             "language": {
                   "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/German.json"
              }
         });

    } );

     jQuery(document).ready(function($) {
         $(".clickable-row").click(function() {
             window.location = $(this).data("href");
         });
     });
</script>
