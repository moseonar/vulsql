<?php
    include "menu.php";
    $info=($user["ID"] == 0 ? "Neuen User anlegen": "User '". $user["LOGIN"]. "' editieren");
    $ticketGroups = (new TicketGroup())->getAllTicketGroups();
    $userGroups = (new UserGroup())->getAllUsersGroups(true);
    $users=(new User())->getAllUsers(true);

    $showDelete = (count($users) <2 ? false :true);
    if(isset($user["_USER_GROUP"]) &&  $user["_USER_GROUP"]["ACCESS"] == ACCESS_TYPE["admin"] && $showDelete) $showDelete = (new User())->enoughAdmins();
    if ($user["ID"]==0) $showDelete=false;
?>
<div class="container">
    <form class="form-horizontal" role="form" id="user" name="user" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="user">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <h3><?php echo $info; ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="DATE_MODIFY" class="col-md-2 control-label">ID</label>
                            <div class="col-md-3">
                              <input readonly="readonly" id="ID" type="text" class="form-control" name="ID" value="<?php echo $user["ID"]; ?>">
                            </div>
                            <label for="DATE_CREATE" class="col-md-2 control-label col-md-offset-2">Erstellt</label>
                            <div class="col-md-3" >

                              <input disabled id="DATE_CREATE" type="text" class="form-control text-center" name="DATE_CREATE" value="<?php echo getDateFromMysql($user["DATE_CREATE"]) ?>">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="LOGIN" class="col-md-2 control-label">Login Name</label>
                            <div class="col-md-10">

                              <input <?php echo ($user["ID"] != 0 ? 'readonly="readonly"' : '');  ?>  autocomplete="off" id="LOGIN" type="text" class="form-control" name="LOGIN" value="<?php echo $user["LOGIN"]; ?>" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="FULL_NAME" class="col-md-2 control-label">Kompletter Name</label>

                            <div class="col-md-10">
                              <input autocomplete="off" id="FULL_NAME" type="text" class="form-control" name="FULL_NAME" value="<?php echo $user["FULL_NAME"]; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EMAIL" class="col-md-2 control-label">E-Mail</label>
                            <div class="col-md-10">
                              <input autocomplete="off" id="EMAIL" type="email" class="form-control" name="EMAIL" value="<?php echo $user["EMAIL"]; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="TICKET_GROUP_ID" class="col-md-2 control-label">Ticket Gruppe</label>
                            <div class="col-md-10">
                                <select class="form-control" name="TICKET_GROUP_ID">
                                <?php
                                    foreach ($ticketGroups as $ticketGroup) {
                                        $selected = ($ticketGroup["ID"] == $user["TICKET_GROUP_ID"]?"selected":"");
                                        echo '
                                            <option '.$selected.' value="'.$ticketGroup["ID"].'">(' . $ticketGroup["ACCESS"] . ') '.$ticketGroup["NAME"].'</option>
                                        ';
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="USER_GROUP_ID" class="col-md-2 control-label">User Gruppe</label>
                            <div class="col-md-10">
                                <select <?php echo (isset($user["_USER_GROUP"]) &&  $user["_USER_GROUP"]["ACCESS"] == ACCESS_TYPE["admin"]?'readonly="readonly"':"") ; ?>  class="form-control" name="USER_GROUP_ID">
                                <?php
                                    foreach ($userGroups as $userGroup) {
                                        $selected = ($userGroup["ID"] == $user["USER_GROUP_ID"]?"selected":"");
                                        echo '
                                            <option '.$selected.' value="'.$userGroup["ID"].'">'.$userGroup["NAME"].'</option>
                                        ';
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="SORT" class="col-md-2 control-label">Sortierung</label>
                            <div class="col-md-3">
                              <input  type="number" min="0" max="999" title="Format: 3 digits" id="SORT" class="form-control " name="SORT" value="<?php echo $user["SORT"];?>" required >
                            </div>
                        </div>
                        <div class="form-group">

                            <label for="PASSWORD" class="col-md-2 control-label">Passwort</label>
                            <div class="col-md-3">
                              <input autocomplete="off" id="PASSWORD" type="password" class="form-control" name="PASSWORD" value="" <?php echo ($user["ID"]==0?"required":"");  ?> >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 pull-right">
                                <?php
                                    $buttons[2]["show"] = true;
                                    $buttons[1]["show"] = $showDelete;
                                    $buttons[1]["class"] = "confirm btn btn-warning";
                                    $buttons[1]["link"] = "href='#'";
                                    $buttons[0]["link"] = "href='index.php?user'";
                                    include "buttons.php";
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" charset="utf-8">
    $(function() {
        $('.confirm').click(function() {
            return window.confirm("User wirklich entfernen?\n(Zugewiesen Tickets verbleiben im System)");
        });
    });
</script>
