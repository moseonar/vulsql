
<?php
  include "menu.php";
  $disabled = ($ticket["ID"] == 0 ? '': 'readonly="readonly"');
  $users = (new User())->getAllUsers(false,true);
  $ticketStatus = (new TicketStatus())->getAllTicketStatus();
  $ticketGroups = (new TicketGroup())->getAllTicketGroups();
  $gesMinuten = (new Ticket())->getAllMinutesForTicket($ticket["ID"])[0]["MINUTES"];
  $gesZeit=str_pad(floor($gesMinuten/60),2,'0',STR_PAD_LEFT).":".str_pad(($gesMinuten%60),2,'0',STR_PAD_LEFT)." (hh:mm)";


  $ticketDATE_MODIFY = ($ticket["DATE_MODIFY"]=="" ? "" :date('d.m.y H:i', strtotime($ticket["DATE_MODIFY"])));
  $ticketDATE_CREATE = ($ticket["DATE_CREATE"]=="" ? "" :date('d.m.y H:i', strtotime($ticket["DATE_CREATE"])));

  $ticketsSub = ( $ticket["ID"] !=0 ? (new Ticket())->getTicketTreeForTicketId($ticket["ID"]) : array() ) ;
  $ticketMaster = ($ticket["PARENT"] != 0 ? (new Ticket())->getTicketForTicketId($ticket["PARENT"]) : array()   );

?>


<div class="container">
    <form class="form-horizontal" role="form" id="ticket" name="ticket" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="ticket">
    
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading ">
                        <div class="form-group">
                            <label for="ID" class="col-md-1 control-label">ID</label>
                            <div class="col-md-3">
                                <input readonly="readonly" id="ID" type="text" class="form-control text-right" name="_ID" value="<?php echo $ticket["ID"]; ?>">
                            </div>
                            <label for="ID" class="col-md-2 col-md-offset-3 control-label">Ges. Zeit</label>
                            <div class="col-md-3">
                                <input readonly="readonly" id="gesZeit" type="text" class="form-control text-right" name="_gesZeit" value="<?php echo $gesZeit; ?>">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="DATE_MODIFY" class="col-md-1 control-label">L.Zugr.</label>
                            <div class="col-md-3">

                              <input readonly="readonly" id="DATE_MODIFY" type="text" class="form-control text-center" name="_DATE_MODIFY" value="<?php echo $ticketDATE_MODIFY; ?> ">
                            </div>
                            <label for="DATE_CREATE" class="col-md-1 control-label">Erstellt</label>
                            <div class="col-md-3">
                              <input readonly="readonly" id="DATE_CREATE" type="text" class="form-control text-center" name="_DATE_CREATE" value="<?php echo $ticketDATE_CREATE; ?> ">
                            </div>
                            <label for="USER_CREATE_ID" class="col-md-1 control-label">Ersteller</label>
                            <div class="col-md-3">
                              <input readonly="readonly" id="USER_CREATE_ID" type="text" class="form-control" name="_USER_CREATE_ID" value="<?php echo (new User())->getUsersForId($ticket["USER_CREATE_ID"])["FULL_NAME"]; ?> ">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">

                      <!--       MASTER TICKET      -->
                      <div class="form-group">
                          <label style="color:grey;" for="MASTERTICKET" class="col-md-1 control-label"><br><br><br>Master</label>
                          <div class="col-md-11 well">
                            <table class="table table-hover" id="SUBTICKETS">
                                <thead>
                                  <tr>
                                    <th width=45px style="color:grey;">Id</th>
                                    <th width=125px style="color:grey;">Datum</th>
                                    <th style="color:grey;">Name</th>
                                    <th width=100px style="color:grey;">Status</th>
                                    <th width=100px style="color:grey;">Gruppe</th>
                                    <th width=100px style="color:grey;">Zust&auml;ndig</th>
                                    <th width=80px style="color:grey;">Prio</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    if (isset($ticketMaster["ID"]) and $ticketMaster["ID"] >0 ) {
                                      $userName= ($ticketMaster["USER_ID"]!=0 ? (new User())->getUsersForId($ticketMaster["USER_ID"])["FULL_NAME"] : "***");
                                      $ticketStatusSub = (new TicketStatus())->getTicketStatusForId($ticketMaster["TICKET_STATUS_ID"]);
                                      $ticketGroupSub = (new TicketGroup())->getTicketGroupForId($ticketMaster["TICKET_GROUP_ID"]);
                                      $prio = ($ticketMaster["PRIO"] == 0 ? "Keine": PRIO[$ticketMaster["PRIO"]]);
                                      $seenColor = ($ticketMaster["SEEN"]==0 ? COLORS["40"]:COLORS["0"]);
                                      echo '
                                      <tr data-toggle="collapse" data-target="#accordion" class="clickable-row '.$seenColor.'" data-href="index.php?showTicketId='.$ticketMaster["ID"].'" >
                                      <td width=45px>'.$ticketMaster["ID"].'</td>
                                      <td width=125px>'. date('d.m.y H:i', strtotime($ticketMaster["REMIND"])).'</td>
                                      <td>'.$ticketMaster["NAME"].'</td>
                                      <td width=100px>'. $ticketStatusSub["NAME"].'</td>
                                      <td width=100px>'.  $ticketGroupSub["NAME"]  .'</td>
                                      <td width=100px>'. $userName.'</td>
                                      <td  width=80px class="'.COLORS[$ticketMaster["PRIO"]].'">'. $prio.'</td>
                                      </tr>';
                                    }
                                  ?>
                                  </tbody>
                                </table>
                          </div>
                      </div>

                      <!--       SUB SUBTICKETS      -->
                        <div class="form-group">
                            <label style="color:grey;" for="SUBTICKETS" class="col-md-1 control-label"><br>Subt.</label>
                            <div class="col-md-11 well">
                                  <table class="table table-hover" id="SUBTICKETS">
                                      <tbody>
                                          <?php
                                          foreach ($ticketsSub as $ticketSub) {
                                              $userName= ($ticketSub["USER_ID"]!=0 ? (new User())->getUsersForId($ticketSub["USER_ID"])["FULL_NAME"] : "***");
                                              $ticketStatusSub = (new TicketStatus())->getTicketStatusForId($ticketSub["TICKET_STATUS_ID"]);
                                              $ticketGroupSub = (new TicketGroup())->getTicketGroupForId($ticketSub["TICKET_GROUP_ID"]);
                                              $prio = ($ticketSub["PRIO"] == 0 ? "Keine": PRIO[$ticketSub["PRIO"]]);
                                              $seenColor = ($ticketSub["SEEN"]==0 ? COLORS["40"]:COLORS["0"]);
                                              echo '
                                              <tr data-toggle="collapse" data-target="#accordion" class="clickable-row '.$seenColor.'" data-href="index.php?showTicketId='.$ticketSub["ID"].'" >
                                              <td width=45px>'.$ticketSub["ID"].'</td>
                                              <td width=125px>'. date('d.m.y H:i', strtotime($ticketSub["REMIND"])).'</td>
                                              <td>'.$ticketSub["NAME"].'</td>
                                              <td width=100px>'. $ticketStatusSub["NAME"].'</td>
                                              <td width=100px>'.  $ticketGroupSub["NAME"]  .'</td>
                                              <td width=100px>'. $userName.'</td>
                                              <td  width=80px class="'.COLORS[$ticketSub["PRIO"]].'">'. $prio.'</td>
                                              </tr>';
                                          }
                                         ?>
                                     </tbody>
                                 </table>
                            </div>
                        </div>


                        <!--       1. ZEILE      -->
                        <div class="form-group">
                            <label for="REMIND" class="col-md-1 control-label">Wiedervorl.</label>
                            <div class="col-md-3">
                                <div class='input-group date' id='REMIND'>
                                   <input name="REMIND" type='text' class="form-control" value="<?php echo getDateFromMysql($ticket["REMIND"]); ?>"/>
                                   <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                   </span>
                                </div>
                            </div>

                            <label for="DATE_READY" class="col-md-1 control-label">Fertig</label>
                            <div class="col-md-3">
                                <div class='input-group date' id='DATE_READY'>
                                    <input name="DATE_READY" type='text' class="form-control" value="<?php echo getDateFromMysql($ticket["DATE_READY"]); ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="PRIO" class="col-md-1 control-label">Prio</label>
                            <div class="col-md-3">
                                    <select class="form-control" name="PRIO">
                                        <?php
                                        foreach (PRIO as $prio=>$key) {
                                            $selected = ($prio == $ticket["PRIO"]?"selected":"");
                                            if($key == PRIO[0] ) $key = "Keine";
                                            echo '<option '.$selected.' value="'.$prio.'">'.$key .'</option>';
                                        }
                                    ?>
                                    </select>

                            </div>

                        </div>

                        <!--       2. ZEILE      -->
                        <div class="form-group">
                            <label for="TICKET_STATUS_ID" class="col-md-1 control-label">Status</label>
                            <div class="col-md-3">
                                <select class="form-control" name="TICKET_STATUS_ID">
                                <?php
                                  foreach ($ticketStatus as $ticketStati) {
                                      $selected=((new TicketStatus())->getTicketStatusForId($ticket["TICKET_STATUS_ID"])["ID"] == $ticketStati["ID"]?"selected":"");
                                      echo '
                                          <option value="'.$ticketStati["ID"].'" '.$selected.'>'.$ticketStati["NAME"].'</option>
                                      ';
                                  }
                                ?>
                                </select>
                            </div>
                            <label for="TICKET_GROUP_ID" class="col-md-1 control-label">Gruppe</label>
                            <div class="col-md-3">
                                <select class="form-control" name="TICKET_GROUP_ID">
                                <?php
                                  foreach ($ticketGroups as $ticketGroup) {
                                      $selected=((new TicketGroup())->getTicketGroupForId($ticket["TICKET_GROUP_ID"])["ID"] == $ticketGroup["ID"]?"selected":"");
                                      echo '
                                          <option value="'.$ticketGroup["ID"].'" '.$selected.'>'.$ticketGroup["NAME"].'</option>
                                      ';
                                  }
                                ?>
                                </select>
                            </div>
                            <label for="USER_ID" class="col-md-1 control-label">Zust&auml;ndig</label>
                            <div class="col-md-3">
                                <select class="form-control"  name="USER_ID">
                                <option value="0">Alle</option>
                                <?php
                                foreach ($users as $user) {
                                    $selected=($ticket["USER_ID"] == $user["ID"]?"selected":"");
                                    echo '
                                        <option value="'.$user["ID"].'" '.$selected.'> '.$user["FULL_NAME"].'</option>
                                    ';
                                }
                                ?>
                                </select>
                            </div>
                        </div>

                        <!--       3. ZEILE      -->
                        <div class="form-group">
                            <label for="NAME" class="col-md-1 control-label">Name</label>
                            <div class="col-md-11">
                                <input  autocomplete="off" id="NAME" type="text" class="form-control " name="NAME" value="<?php echo $ticket["NAME"]; ?>"   required  autofocus  >
                            </div>
                        </div>

                        <!--       3. ZEILE  (NEUES TICKT DATA)    -->
                        <hr>
                        <div class="form-group">
                            <label for="SUBJECT" class="col-md-1 control-label">Betreff</label>
                            <div class="col-md-7">
                                <input autocomplete="off" id="SUBJECT" type="text" class="form-control " name="SUBJECT" value="" >
                            </div>
                            <label for="MINUTES" class="col-md-1 control-label">Minuten</label>
                            <div class="col-md-3">
                                <input autocomplete="off" type="number" min="0" max="999" title="Format: 3 digits" id="MINUTES" class="form-control " name="MINUTES" value="0" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="PROCESS" class="col-md-1 control-label">Prozess</label>
                            <div class="col-md-11">
                                <textarea name="PROCESS" class="form-control" rows="6" id="PROCESS" autofocus required></textarea>
                            </div>
                        </div>

                        <!--       5. ZEILE Button      -->
                        <div class="form-group">

                          <div class="col-md-6  col-md-offset-2  <?php if ($ticket["ID"] != 0)  echo "well"; ?>">
                            <?php
                              if ($ticket["ID"] != 0) {
                                if (isset($_SESSION["ot"]["SET_MASTER_TICKET_FOR"]) and $_SESSION["ot"]["SET_MASTER_TICKET_FOR"] >0) {
                                    echo '
                                      <a href="index.php?ADD_MASTER_TICKET_FOR='.$ticket["ID"].'" class="btn btn-danger" type="submit" name="BUTTON_ADD_MASTER_TICKET">
                                          Ticket ID '.$_SESSION["ot"]["SET_MASTER_TICKET_FOR"].' unterordnen unter ID '.$ticket["ID"].'<i class="glyphicon glyphicon glyphicon-save-file"></i>
                                      </a>
                                    ';
                                } else {
                                  echo '
                                  <a href="index.php?ADD_SUBTICKET_FOR_MASTER_TICKET='.$ticket["ID"].'" class="btn btn-danger" type="submit" name="ADD_SUBTICKET_FOR_MASTER_TICKET">
                                      Neues Sub-Ticket anlegen <i class="glyphicon  glyphicon-open-file"></i>
                                  </a>

                                    <a href="index.php?SET_MASTER_TICKET_FOR='.$ticket["ID"].'" class="btn btn-danger" type="submit" name="BUTTON_SET_MASTER_TICKET">
                                        Dieses Ticket unterordnen <i class="glyphicon glyphicon-save-file"></i>
                                    </a>
                                  ';
                                }
                              }
                             ?>
                           </div>

                            <div class="col-md-4 ">
                                <?php
                                    $buttons[2]["show"] = true;
                                    include "buttons.php";
                                 ?>
                            </div>
                        </div>


                        <hr>
                        <?php
                            $ticketdatas = (new TicketData())->getAllTicketDataForTicketId($ticket["ID"]);

                            foreach ($ticketdatas as $ticketdata) {
                                $kopf="Erstellt: ". date('d.m.y H:i', strtotime($ticketdata["DATE_CREATE"])) . "; Ersteller: ".(new User())->getUsersForId($ticketdata["USER_ID"])["FULL_NAME"]
                                . "; Minuten: " . $ticketdata["MINUTES"]. "; Betreff: " . $ticketdata["SUBJECT"] ;
                                echo '
                                    <!--
                                    <div class="form-group">
                                        <label for="SUBJECT1" class="col-md-1 control-label">Betreff</label>
                                        <div class="col-md-7">
                                            <input readonly="readonly" autocomplete="off" id="SUBJECT1" type="text" class="form-control " name="_SUBJECT[]" value="'.$ticketdata["SUBJECT"].'">
                                        </div>
                                        <label for="DATE_CREATE1" class="col-md-1 control-label">Erstellt</label>
                                        <div class="col-md-3">

                                          <input readonly="readonly" id="DATE_CREATE1" type="text" class="form-control text-center" name="_DATE_CREATE1[]" value="'.date('d.m.y H:i', strtotime($ticketdata["DATE_CREATE"])).'">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="TICKET_DATA_ID" class="col-md-1 control-label">ID</label>
                                        <div class="col-md-3">
                                            <input readonly="readonly" id="TICKET_DATA_ID" type="text" class="form-control" name="_TICKET_DATA_ID[]" value="'.$ticketdata["ID"].'">
                                        </div>

                                        <label for="USER_ID_CREATED" class="col-md-1 control-label">Ersteller</label>
                                        <div class="col-md-3">
                                            <input readonly="readonly" id="USER_ID_CREATED" type="text" class="form-control " name="_USER_ID_CREATED[]" value="'.(new User())->getUsersForId($ticketdata["USER_ID"])["FULL_NAME"].'">
                                        </div>

                                        <label for="MINUTES" class="col-md-1 control-label">Minuten</label>
                                        <div class="col-md-3">
                                            <input readonly="readonly" id="MINUTES" type="text" class="form-control " name="_MINUTES[]" value="'.$ticketdata["MINUTES"].'">
                                        </div>
                                    </div>
                                    -->
                                    <div class="form-group">
                                        <label style="color:grey;" for="PROCESS" class="col-md-1 control-label">Prozess</label>
                                        <div class="col-md-11">
                                            ';
                                            if (trim($ticketdata["PROCESS"]) !="")   {
                                                    echo '<textarea readonly="readonly" class="form-control" rows="8" id="_PROCESS">'.$ticketdata["PROCESS"].'</textarea>';
                                            } else {
                                                echo '<input readonly="readonly" id="_PROZESS" type="text" class="form-control " name="_PROZESS" value="">';
                                            }

                                            echo '
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label style="color:grey;" for="LOG" class="col-md-1 control-label">Log</label>
                                        <div class="col-md-11">

                                            <input style="color:grey;" readonly="readonly" autocomplete="off" id="LOG" type="text" class="form-control " name="_LOG" value="'.$kopf. " " .$ticketdata["LOG"].'">

                                        </div>
                                    </div>
                                    <hr>
                                ';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" charset="utf-8">
    $(function () {
        <!-- https://github.com/AuspeXeu/bootstrap-datetimepicker -->
        $('#REMIND').datetimepicker({
            locale: 'de',
            format: 'DD.MM.YY HH:mm',
        });
        $('#DATE_READY').datetimepicker({
            locale: 'de',
            format: 'DD.MM.YY HH:mm',
        });

    });

    $.getJSON('/ot/include/fetch.php?id=<?php echo $ticket["ID"]; ?>', function(data) {
      //console.log(data);
      //<div id="tree"></div>
      $('#tree').treeview({data: data});
    });

    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });

</script>
