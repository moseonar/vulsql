<?php include "menu.php" ?>
<div class="container">
    <form class="form-horizontal" role="form" id="home" name="home" method="POST" action="index.php" >
        <input type="hidden" name="_FORM" value="home">

        <div class="row">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <div class="col-md-12" ><br>
                                <button style="overflow: visible !important; height: 0 !important; width: 0 !important; margin: 0 !important; border: 0 !important; padding: 0 !important; display: block !important;" name="DEFAULT_SUBMIT_BUTTON" type="submit" value="default action"/>
                                <button class="btn btn-primary" type="submit" name="HOME">
                                    <i class="glyphicon glyphicon-home"></i> Home view
                                </button>

                                <button class="btn btn-primary" type="submit" name="ADD_TICKET">
                                    <i class="glyphicon glyphicon-plus"></i> Neues Ticket hinzuf&uuml;gen
                                </button>
                                <?php
                                if (isset($_SESSION["ot"]["SET_MASTER_TICKET_FOR"]) and $_SESSION["ot"]["SET_MASTER_TICKET_FOR"] >0) {
                                  echo '<a href="index.php?BREAK_SET_MASTER_TICKET_FOR" class="btn btn-danger" type="submit" name="BUTTON_SET_MASTER_TICKET">
                                          Ticket ID:'.$_SESSION["ot"]["SET_MASTER_TICKET_FOR"].' unterordnen abbrechen<i class="glyphicon glyphicon-tree-conifer"></i>
                                        </a>
                                        ';
                                }
                                ?>
                            </div>

                        </div>
                    </div>

                    <div class="panel-body">





                        <div class="form-group">

                            <div class="col-md-2">
                                Anzeige
                                <select class="form-control" name="display" onchange="this.form.submit()">
                                <?php
                                    foreach (DISPLAY as $display=>$key) {
                                        $selected = ($display == $route["display"]?"selected":"");
                                        echo '
                                            <option '.$selected.' value="'.$display.'">'.$key.'</option>
                                        ';
                                    }
                                ?>
                                </select>

                            </div>
                            <div class="col-md-2">
                                Status
                                <select class="form-control" name="STATUS_ID" onchange="this.form.submit()">
                                <option value="0">Alle ausser *</option>
                                <?php
                                    foreach ($ticketStatus as $ticketStati) {
                                        $selected = ($ticketStati["ID"] == $route["STATUS_ID"]?"selected":"");
                                        $dot= ($ticketStati["CLOSED"] ==1 ?"*":"");
                                        echo '
                                            <option '.$selected.' value="'.$ticketStati["ID"].'">'.$dot.$ticketStati["NAME"].'</option>
                                        ';
                                    }
                                ?>
                                </select>


                            </div>
                            <div class="col-md-2 col-md-offset-1">
                                Gruppe
                                <select class="form-control" name="GROUP_ID" onchange="this.form.submit()">
                                <option value="0">Alle</option>
                                <?php
                                    foreach ($ticketGroups as $ticketGroup) {
                                        $selected = ($ticketGroup["ID"] == $route["GROUP_ID"]?"selected":"");
                                        echo '
                                            <option '.$selected.' value="'.$ticketGroup["ID"].'">(' . $ticketGroup["ACCESS"] . ') '.$ticketGroup["NAME"].'</option>
                                        ';
                                    }
                                ?>
                                </select>

                            </div>
                            <div class="col-md-2 col-md-offset-1">
                                Zust&auml;ndig
                                <select class="form-control" name="USER_ID"  onchange="this.form.submit()">
                                    <option value="0">Alle</option>
                                    <?php
                                    foreach ($users as $user) {
                                        $selected = ($user["ID"] == $route["USER_ID"]?"selected":"");
                                        echo '<option '.$selected.' value="'.$user["ID"].'">'. $user["FULL_NAME"].'</option>';
                                    }
                                ?>
                                </select>

                            </div>
                            <div class="col-md-2 ">
                                Priorit&auml;t
                                <select class="form-control" name="PRIO"  onchange="this.form.submit()">
                                    <?php
                                    foreach (PRIO as $prio=>$key) {
                                        $selected = ($prio == $route["PRIO"]?"selected":"");
                                        echo '<option '.$selected.' value="'.$prio.'">'.$key .'</option>';
                                    }
                                ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-md-2">
                                ID
                                <input name="ID_SEARCH" type='text' class="form-control" value="<?php echo $route["ID_SEARCH"] ?>"/>
                            </div>

                            <div class="col-md-2 ">
                                Von
                                <div class='input-group date' id='FROM'>
                                   <input name="FROM" type='text' class="form-control" value="<?php echo $route["FROM"] ?>"/>
                                   <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                   </span>
                                </div>
                            </div>


                            <div class="col-md-2">
                                Bis
                                <div class='input-group date' id='UNTIL'>
                                    <input name="UNTIL" type='text' class="form-control" value="<?php echo $route["UNTIL"] ?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                Volltextsuche
                                <input name="FULLTEXT" type='text' class="form-control" value="<?php echo $route["FULLTEXT"] ?>"/>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <button class="btn btn-success" type="submit" name="BUTTON_FILTER">
                                    <i class="glyphicon glyphicon-search"></i> Filter
                                </button>
                                <button class="btn btn-warning" type="submit" name="BUTTON_FILTER_RESET">
                                    <i class="glyphicon glyphicon-reset"></i> Reset
                                </button>


                            </div>
                        </div>



                        <div>
                            <hr>
                        </div>




                        <div class="form-group">
                            <div class="col-md-12">
                                <table class="table table-hover" id="tickets">
                                    <thead>
                                      <tr>
                                        <th width=45px>Id</th>
                                        <th width=125px>Datum</th>
                                        <th >Name</th>
                                        <th width=100px>Status</th>
                                        <th width=100px>Gruppe</th>
                                        <th width=100px>Zust&auml;ndig</th>
                                        <th width=70px>Prio</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($tickets as $ticket) {
                                            $userName= ($ticket["USER_ID"]!=0 ? (new User())->getUsersForId($ticket["USER_ID"])["FULL_NAME"] : "***");
                                            $ticketStatus = (new TicketStatus())->getTicketStatusForId($ticket["TICKET_STATUS_ID"]);
                                            $ticketGroup = (new TicketGroup())->getTicketGroupForId($ticket["TICKET_GROUP_ID"]);
                                            $prio = ($ticket["PRIO"] == 0 ? "Keine": PRIO[$ticket["PRIO"]]);
                                            $seenColor = ($ticket["SEEN"]==0 ? COLORS["40"]:COLORS["0"]);
                                            echo '
                                            <tr data-toggle="collapse" data-target="#accordion" class="clickable-row '.$seenColor.'" data-href="index.php?showTicketId='.$ticket["ID"].'" >
                                            <td>'.$ticket["ID"].'</td>
                                            <td>'. date('d.m.y H:i', strtotime($ticket["REMIND"])).'</td>
                                            <td>'.$ticket["NAME"].'</td>
                                            <td>'. $ticketStatus["NAME"].'</td>
                                            <td>'.  $ticketGroup["NAME"]  .'</td>
                                            <td>'. $userName.'</td>
                                            <td class="'.COLORS[$ticket["PRIO"]].'">'. $prio.'</td>
                                            </tr>';
                                        }
                                       ?>
                                   </tbody>
                               </table>
                           </div>
                        </div>
                    </div>

            </div>
        </div>
    </form>
</div>

<!--
    https://datatables.net/examples/plug-ins/range_filtering.html

-->
<script type="text/javascript" charset="utf-8">
     $(document).ready(function() {
         $.fn.dataTable.moment( 'DD.MM.YY HH:mm' );
          $('#tickets').DataTable({
               "order": [[ 1, "desc" ]],
              "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.10/i18n/German.json"
               }
          });

     } );
     jQuery(document).ready(function($) {
         $(".clickable-row").click(function() {
             window.location = $(this).data("href");
         });
     });
     $(function () {
         <!-- https://github.com/AuspeXeu/bootstrap-datetimepicker -->
         $('#FROM').datetimepicker({
             locale: 'de',
             format: 'DD.MM.YY',
         });
         $('#UNTIL').datetimepicker({
             locale: 'de',
             format: 'DD.MM.YY',
         });

     });

</script>
