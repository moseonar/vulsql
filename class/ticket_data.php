<?php

class TicketData {


    // private $ID=0;
    // private $TICKET_ID=0;
    // private $NAME="";
    // private $PROCESS="";
    // private $MINUTES=0;
    // private $DATE_CREATE = null;
    // private $DATE_MODIFY = null;


    public function getAllTicketDataForTicketId($id) {
        global $dbprefix;
        $sql="select * from ".$dbprefix."ticket_data where TICKET_ID='".$id."' order by ID DESC";
        return getArrayFromDB($sql);
    }

    public function getTicketDataForId($id) {
        global $dbprefix;
        $sql="select * from ".$dbprefix."ticket_data where ID='".$id."'";
        return getRowFromDB($sql);
    }

    public function saveTicketData($post) {
        //nur hinzufügen, kein UPDATE
        $ticketData = array();
        $vars=array("SUBJECT", "PROCESS","MINUTES");
        foreach ($vars as $var) {
          $ticketData[$var] = $post[$var];
        }
        $ticketData["TICKET_ID"] = $post["_ID"];
        $ticketData["USER_ID"] = $_SESSION['ot']["user"]["ID"];
        $ticketData["LOG"] = $this->createLog($post);
        $sql=createSQL("ticket_data", $ticketData, ADD_DATA_TO_DATABASE);
        $lid=executeSQL($sql);
    }

    private function createLog($post){

        $oldTicket = (new Ticket())->getTicketForTicketId($post["_ID"]);

        $log="";
        if (isset($post["TICKET_STATUS_ID"]) && $oldTicket["TICKET_STATUS_ID"] != $post["TICKET_STATUS_ID"]) {
            $alt = (new TicketStatus())->getTicketStatusForId($oldTicket["TICKET_STATUS_ID"])["NAME"];
            $neu = (new TicketStatus())->getTicketStatusForId($post["TICKET_STATUS_ID"])["NAME"];
            $log = $log . "Status: " . $alt ."->". $neu."; ";
        }

        if (isset($post["TICKET_GROUP_ID"]) &&  $oldTicket["TICKET_GROUP_ID"] != $post["TICKET_GROUP_ID"]) {
            $alt = (new TicketGroup())->getTicketGroupForId($oldTicket["TICKET_GROUP_ID"])["NAME"];
            $neu = (new TicketGroup())->getTicketGroupForId($post["TICKET_GROUP_ID"])["NAME"];
            $log = $log . "Group: " . $alt ."->". $neu."; ";
        }
        if (isset($post["USER_ID"]) && $oldTicket["USER_ID"] != $post["USER_ID"]) {
            $alt = (new User())->getUsersForId($oldTicket["USER_ID"])["FULL_NAME"];
            $neu = (new User())->getUsersForId($post["USER_ID"])["FULL_NAME"];
            $log = $log ."User: ". $alt ."->". $neu."; ";
        }
        if (isset($post["DATE_READY"]) &&  getDateFromMysql($oldTicket["DATE_READY"]) != $post["DATE_READY"] && $oldTicket["DATE_READY"] != "1970-01-01 00:00:00") {
            $alt = ($oldTicket["DATE_READY"] ==""?"": getDateFromMysql($oldTicket["DATE_READY"]));
            $neu = $post["DATE_READY"];
            $log = $log . "Fertig: " . $alt ."->". $neu."; ";
        }
        if (isset($post["REMIND"]) && getDateFromMysql($oldTicket["REMIND"]) != $post["REMIND"] && $oldTicket["REMIND"] != "1970-01-01 00:00:00") {
            $alt = ($oldTicket["REMIND"] ==""?"": getDateFromMysql($oldTicket["REMIND"]));
            $neu = $post["REMIND"];
            $log = $log . "Wiedervorl.: " . $alt ."->". $neu."; ";
        }
        if (isset($post["NAME"]) && trim($oldTicket["NAME"]) != trim($post["NAME"])) {
            $alt = $oldTicket["NAME"];
            $neu = $post["NAME"];
            $log = $log . 'Name: `'. $alt .'`->`'. $neu.'`; ';
        }
        if (isset($post["PRIO"]) &&  trim($oldTicket["PRIO"]) != trim($post["PRIO"])) {
            $alt = PRIO[$oldTicket["PRIO"]];
            $neu = PRIO[$post["PRIO"]];
            $log = $log . 'Prio: `'. $alt .'`->`'. $neu.'`; ';
        }
        if (isset($post["SEEN"]) &&  trim($oldTicket["SEEN"]) != trim($post["SEEN"])) {
            $alt = $oldTicket["SEEN"];
            $neu = $post["SEEN"];
            $log = $log . 'Seen: `'. $alt .'`->`'. $neu.'`; ';
        }
        return trim($log);
    }
}
