<?php

class User {
    public function getNewUsers() {
        $user=array();
        $user["ID"]=0;
        $user["SORT"]=10;
        $user["LOGIN"]="";
        $user["FULL_NAME"]="";
        $user["EMAIL"]="";
        $user["DATE_CREATE"] = date("Y-m-d H:i:s");;
        return $user;
    }

    public function delete($id) {
        global $dbprefix;
        $sql="delete  from ".$dbprefix."user where ID='".$id."'";
        executeSQL($sql);
    }
    public function enoughAdmins() {
        global $dbprefix;
        $sql='select ID from '.$dbprefix.'user_group where ACCESS="'. ACCESS_TYPE["admin"] . '"';
        $adminGroupId=getRowFromDb($sql);
        $sql='select count(*) as count from '.$dbprefix.'user where USER_GROUP_ID="'. $adminGroupId["ID"] . '"';
        $admincount=getRowFromDb($sql);
        if ($admincount["count"] > 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllUsers($filer=false,$withoutAdmin=false) {
        global $dbprefix;

        if  ($filer && (new UserGroup())->isAdmin( $_SESSION['ot']["user"]["ID"])) {
            $sql="select * from ".$dbprefix."user where ID ='".$_SESSION['ot']["user"]["ID"]."'";
        } else {
            $sql="select * from ".$dbprefix."user order by `SORT`,`FULL_NAME`";
        }

        $users=  getArrayFromDb($sql);
        $newUsers=array();
        foreach ($users as $user) {
            $user["UserGroup"]=(new UserGroup())->getUsersGroupForId($user["USER_GROUP_ID"]);
            if (! $filer){

                 if  ($withoutAdmin and (new UserGroup())->isAdmin( $user["ID"])) {} else {
                     $newUsers[]=$user;
                 }
            } else {
                if($user["UserGroup"]["ACCESS"] <= $_SESSION['ot']["user"]["ACCESS"]) $newUsers[]=$user;
            }

        }
        return $newUsers;
    }
    
    public function getUsersForId($id,$userOnly=false) {
        global $dbprefix;
        $sql="select * from ".$dbprefix."user where ID='".$id."'";
        $user=getRowFromDb($sql);
        if(! $userOnly) {
            $sql="select * from ".$dbprefix."user_group where ID='".$user["USER_GROUP_ID"]."'";
            $userGroup=getRowFromDb($sql);
            $user["_USER_GROUP"] = $userGroup;
        }
        return $user;
    }
    public function searchUsersForLogin($search) {
        global $dbprefix;
        $sql="select * from ".$dbprefix."user where LOGIN LIKE '%".$search."%'";
        return getArrayFromDb($sql);
    }
    public function saveUser($route) {
        $vars=array("LOGIN","FULL_NAME","EMAIL","TICKET_GROUP_ID","USER_GROUP_ID","SORT");
        if ($route["ID"] == 0) {
            $user=array();
            $addUpdate = ADD_DATA_TO_DATABASE;
        } else {
            $user=$this->getUsersForId($route["ID"],true);
            $addUpdate = UPDATE_DATA_TO_DATABASE;
        }
        foreach ($vars as $var) {
            $user[$var] = $route[$var];
        }
        if($route["PASSWORD"]!="") $user["PASSWORD"] = md5($route["PASSWORD"]);

        $sql=createSQL("user", $user, $addUpdate, "ID", $user["ID"]);
        $lid=executeSQL($sql);
    }

}
