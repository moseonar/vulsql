<?php


class Ticket {

	public function getEmptyTicket(){
		$ticket=array();
		$ticket["ID"]=0;
	  $ticket["TICKET_STATUS_ID"] = 0;
	  $ticket["TICKET_GROUP_ID"] = 0;
		$ticket["USER_CREATE_ID"] = 0;
	  $ticket["USER_ID"] = $_SESSION['ot']["user"]["ID"];
		$ticket["NAME"] = "";
		$ticket["REMIND"] = mytoday();
		$ticket["DATE_READY"] = mytoday();
		$ticket["PARENT"] = 0;
		return $ticket;
	}

	public function __construct() {
	}



public function getTicketTreeForTicketId($id){
	global $dbprefix;
	$sql="select * from ".$dbprefix."ticket where PARENT='".$id."' ";
	// $closedTicketStatis=(new TicketStatus())->getAllTicketStatusOpenClosed(1);
	// foreach ($closedTicketStatis as $closedTicketStatus) {
	// 	$sql =  $sql . ' and TICKET_STATUS_ID != "'.$closedTicketStatus["ID"].'"';
	// }

	return getArrayFromDb($sql);
}



	public function getTicketForTicketId($id) {
      global $dbprefix;
      $sql="select * from ".$dbprefix."ticket where ID='".$id."'";
	  	return getRowFromDB($sql);
  }


  public function getAllTickets($route, $tree=false) {

	  global $dbprefix;
	  //old way
	  $statusID=$route["STATUS_ID"];
	  $userID=$route["USER_ID"];
	  $groupID=$route["GROUP_ID"];
	  $prio=$route["PRIO"];
	  $display=$route["display"];

	  if(trim($route["FULLTEXT"])!="") {
		$sql="select distinct
			".$dbprefix."ticket.ID as ID,
			".$dbprefix."ticket.TICKET_STATUS_ID,
			".$dbprefix."ticket.TICKET_GROUP_ID,
			".$dbprefix."ticket.USER_ID,
			".$dbprefix."ticket.USER_CREATE_ID,
			".$dbprefix."ticket.NAME,
			".$dbprefix."ticket.DATE_READY,
			".$dbprefix."ticket.REMIND,
			".$dbprefix."ticket.ACCESS,
			".$dbprefix."ticket.PRIO,
			".$dbprefix."ticket.SEEN,
			".$dbprefix."ticket.DATE_CREATE,
			".$dbprefix."ticket.DATE_MODIFY,
			".$dbprefix."ticket_data.PROCESS,
			".$dbprefix."ticket_data.SUBJECT
			FROM  ".$dbprefix."ticket
		  	LEFT JOIN ".$dbprefix."ticket_data ON (".$dbprefix."ticket.ID = ".$dbprefix."ticket_data.TICKET_ID)
		  	where 1=1
		";
	  } else {
		  $sql="select * from ".$dbprefix."ticket where 1=1 ";
	  }

	  if ($statusID !=0) $sql = $sql . " and ".$dbprefix."ticket.TICKET_STATUS_ID=" . $statusID ;
	  if ($userID !=0) $sql = $sql . " and ".$dbprefix."ticket.USER_ID=" . $userID ;
	  if ($groupID !=0) $sql = $sql . " and ".$dbprefix."ticket.TICKET_GROUP_ID=" . $groupID ;
	  if ($prio !=0) $sql = $sql . " and ".$dbprefix."ticket.PRIO=" . $prio ;

	  if(trim($route["FROM"])!="") {
		  $sql=$sql .' and '.$dbprefix.'ticket.REMIND >= "'.getMysqldateFromInput($route["FROM"]." 00:00").'" ';
	  }
	  if(trim($route["UNTIL"])!="") {
		  $sql=$sql .' and '.$dbprefix.'ticket.REMIND <= "'.getMysqldateFromInput($route["UNTIL"]." 23:59").'" ';
	  }

	  $fulltextSQL=""; $nameSql="";
	  if(trim($route["FULLTEXT"])!="") {
	  		$t=explode(" ",$route["FULLTEXT"]);
	  		$tout=" "; $outSubject = " ";
	  		for ($i=0; $i<count($t); $i++){
	  			if (trim($t[$i])) {
					$nameSql = $nameSql ." ". $dbprefix."ticket.NAME like '%" . $t[$i] . "%' ";
	  				$tout = $tout . " PROCESS like '%" . $t[$i] . "%' ";
	  				$outSubject = $outSubject . " SUBJECT like '%" . $t[$i] . "%' ";
	  				if ($i < count($t) -1 && trim($t[$i+1])){
	  					$tout = $tout . " and ";
	  					$outSubject = $outSubject . " and ";
						$nameSql = $nameSql  . " and ";
	  				}

	  			}
	  		}
	  		//$fulltextSQL = " and ((" . $tout ." ) or  (" . $outSubject . ")  or (" . $nameSql . ") ) ";
			$fulltextSQL = " and ((" . $tout ." ) or  (" . $outSubject . ")  ) ";

			$sql = $sql . $fulltextSQL ;
	  }


	  if ($display == 0) {
		  $dateEnd = date("Y-m-d H:i:s");
	  } else if($display == 10){
		  $dateEnd = date("Y-m-d 23:59:59");
	  } else {
		  $dateEnd="";
	  }
	  if ($dateEnd !="") {
		  $sql = $sql . ' and '.$dbprefix.'ticket.REMIND<="' . $dateEnd.'"' ;
	  }

	  if($statusID == 0){
		  $closedTicketStatis=(new TicketStatus())->getAllTicketStatusOpenClosed(1);
		  foreach ($closedTicketStatis as $closedTicketStatus) {
		  	$sql =  $sql . ' and TICKET_STATUS_ID != "'.$closedTicketStatus["ID"].'"';
		  }
	  }

	  if (isset($route["PARENT_ONLY"])) $sql = $sql . " and PARENT=0 ";
	  if (trim($route["ID_SEARCH"]) != "" ) $sql="select * from ".$dbprefix."ticket where ID = '".$route["ID_SEARCH"]."'";

	  $user=(new User())->getUsersForId($_SESSION["ot"]["user"]["ID"]);
	  $userGroup=(new UserGroup())->getUsersGroupForId($user["USER_GROUP_ID"]);
	  $sql = $sql . ' and '.$dbprefix.'ticket.ACCESS <= '. $userGroup["ACCESS"];

		if (isset($_SESSION["ot"]["SET_MASTER_TICKET_FOR"]) and $_SESSION["ot"]["SET_MASTER_TICKET_FOR"] >0) {
			$sql = $sql . ' and '.$dbprefix.'ticket.ID != "'. $_SESSION["ot"]["SET_MASTER_TICKET_FOR"].'"';
		}
	  $sql = $sql . ' order by '.$dbprefix.'ticket.REMIND';


	  $tickets=getArrayFromDb($sql);

	  $newtickets=array();
	  foreach ($tickets as $ticket) {
		  //$subtickets = $this->subTicket($ticket["ID"]);
		  $subtickets = $this->subTicket(36);
		  if(count($subtickets)>0) $ticket["SUBTICKETS"] = $subtickets;
		  $newtickets[]=$ticket;
	  }
      return $newtickets;
  }


  public function subTicket($id){
	  $sqls='select * from ticket where PARENT = "'.$id.'"';
	  $subtickets = getArrayFromDb($sqls);
	  $ret=array();
	  if (count($subtickets)>0) {
		  foreach ($subtickets as $subticket) {
			  $ret[]=$subticket;
			  $r=$this->subTicket($subticket["ID"]);
			  if (count($r)>0) $ret[]=$r;
		  }
	  }
	  return $ret;
  }

  public function getAllMinutesForTicket($id) {
	  global $dbprefix;
	  $sql="select sum(MINUTES) as MINUTES  from ".$dbprefix."ticket_data where TICKET_ID='".$id."'";
	  return getArrayFromDB($sql);
  }

	public function saveTicket($post) {
	  	if ($post["_ID"] == 0 ) {
			$ticket= $this->getEmptyTicket();
			$ticket["USER_CREATE_ID"] = $_SESSION['ot']["user"]["ID"];
			$addUpdate=ADD_DATA_TO_DATABASE;
		} else {
			(new TicketData())->saveTicketData($post);
			$ticket = $this->getTicketForTicketId($post["_ID"]);
			$addUpdate=UPDATE_DATA_TO_DATABASE;
		}
		if( ($ticket["USER_ID"] != $post["USER_ID"] && $post["_ID"] != 0)) {
			//Remind DATUM Resetten bei User wechsel
			if ($post["USER_ID"] == $_SESSION['ot']["user"]["ID"]){
				//nicht resetten wenn userwehcsel auf angemeldeten User
				$ticket["REMIND"] = getMysqldateFromInput($post["REMIND"]);
			} else {
				$ticket["REMIND"] = date('Y-m-d H:i:s');
			}
		} else {
			//standrad lesen aus Maske
			$ticket["REMIND"] = getMysqldateFromInput($post["REMIND"]);
		}
		$vars=array("TICKET_STATUS_ID", "TICKET_GROUP_ID", "USER_ID", "PRIO", "NAME");
		foreach ($vars as $var) {
		  $ticket[$var] = $post[$var];
		}

		// SEEN FLAG ---------------------------------------------------------------------------
		if (
			//ticket ist an den angemeldeten Benutzer
			($post["USER_ID"] == $_SESSION['ot']["user"]["ID"])
			or
			//ticket ist an den gleichen Benutzer wie vorher und kein neues Ticket
			($post["USER_ID"] == $ticket["USER_ID"] and $ticket["ID"] != 0)
			or
			//ticket ist erledigt oder geloescht
			( (new TicketStatus())->isReady($ticket["TICKET_STATUS_ID"]) )
			)
		{
			$ticket["SEEN"] = 1;
		} else {
			$ticket["SEEN"] = 0;
		}
		// SEEN FLAG ---------------------------------------------------------------------------

		$ticket["DATE_READY"] = getMysqldateFromInput($post["DATE_READY"]);
		$ticket["DATE_MODIFY"] = date('Y-m-d H:i:s');
		$ticket["DATE_READY"] =(trim($ticket["DATE_READY"])==""?"1970-01-01 00:00:00": trim($ticket["DATE_READY"]));

		if($post["USER_ID"] != 0){
			$tuser = (new User())->getUsersForId($post["USER_ID"]);
			$ticket["ACCESS"]=(new UserGroup())->getUsersGroupForId($tuser["USER_GROUP_ID"])["ACCESS"];
		} else {
			$ticket["ACCESS"] = ACCESS_TYPE["default"];
		}
		$sql=createSQL("ticket", $ticket, $addUpdate, "ID", $ticket["ID"]);
		$lid=executeSQL($sql);
		if ($addUpdate == ADD_DATA_TO_DATABASE){
			$ticket = $this->getTicketForTicketId($lid);
			$post["_ID"] = $lid;
			(new TicketData())->saveTicketData($post);
		}

		//unterordnen unter masterticket
		if (isset($_SESSION["ot"]["SET_MASTER_TICKET_FOR"]) and  $_SESSION["ot"]["SET_MASTER_TICKET_FOR"] >0 ) {
			//dd($_SESSION["ot"]["SET_MASTER_TICKET_FOR"]." ".$lid);
			$this->setParentForId($ticket["ID"], $_SESSION["ot"]["SET_MASTER_TICKET_FOR"]);
			$_SESSION["ot"]["SET_MASTER_TICKET_FOR"] = 0;
			unset( $_SESSION["ot"]["SET_MASTER_TICKET_FOR"]);

		}
	  header("Location: index.php");
  }

	public function startGui($route){

		if (! is_array($route)) $route=array();
		if (! isset($route["USER_ID"])) $route["USER_ID"] =0;
		if (! isset($route["STATUS_ID"])) $route["STATUS_ID"] =0;
		if (! isset($route["GROUP_ID"])) $route["GROUP_ID"] =0;
		if (! isset($route["PRIO"])) $route["PRIO"] =0;
		if (! isset($route["display"])) $route["display"] =0;
		if (! isset($route["FROM"])) $route["FROM"] ="";
		if (! isset($route["UNTIL"])) $route["UNTIL"] ="";
		if (! isset($route["FULLTEXT"])) $route["FULLTEXT"] ="";
		if (! isset($route["ID_SEARCH"])) $route["ID_SEARCH"] ="";


		$users = (new User())->getAllUsers(true);
		$ticketStatus = (new TicketStatus())->getAllTicketStatus();

		//$tickets = $this->getAllTickets($route["STATUS_ID"], $route["USER_ID"], $route["GROUP_ID"],$route["PRIO"], $route["display"]);
		$tickets = $this->getAllTickets($route);
		$ticketGroups = (new TicketGroup())->getAllTicketGroups();

		include "ui/home.php";
	}
	public function setSeen($id){
		global $dbprefix;
		$ticket=getRowFromDB("select * from ".$dbprefix."ticket where ID='".$id."'");
		if ($ticket["SEEN"] ==0) {
			$p=array("SUBJECT"=>"","PROCESS"=>"","MINUTES"=>0, "_ID"=>$id, "SEEN"=>1);
			(new TicketData())->saveTicketData($p);
			$sql="update ".$dbprefix."ticket set SEEN='1' where ID='".$id."'";
			$lid = executeSQL($sql);
		}
	}
	public function setParentForId($id,$parent){
		global $dbprefix;
		$sql='update '.$dbprefix.'ticket set PARENT="'.$parent.'" where ID="'.$id.'"';
		executeSQL($sql);
	}

	public function newTicket($route){
		$ticket = (new Ticket())->getEmptyTicket();
		$keinStatus=(new TicketStatus())->searchTicketStatusForName("Offen");
		if (count($keinStatus)>0) {
				$keinStatus=$keinStatus[0];
		} else {
				$keinStatus["ID"]=0;
		}
		$ticket["TICKET_STATUS_ID"]=$keinStatus["ID"];
		$ticket["USER_CREATE_ID"] = $_SESSION["ot"]["user"]["ID"];
		$ticket["DATE_CREATE"] = "";
		$ticket["DATE_MODIFY"] = "";
		return $ticket;

	}

}
