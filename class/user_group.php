<?php

class UserGroup {

    public function isAdmin($id) {
        global $dbprefix;
        $user=(new User())->getUsersForId( $id);

        if (! isset($user["USER_GROUP_ID"])) dd($id,1);
        $sql="select * from ".$dbprefix."user_group where ID ='".$user["USER_GROUP_ID"]."'";

        $userGroup =  getRowFromDb($sql);

        if ($userGroup["ACCESS"]=="999") {
            return true;
        } else {
            return false;
        }
    }

    public function getAllUsersGroups($filter=false) {
        global $dbprefix;
        if (! $filter) {
            $sql="select * from ".$dbprefix."user_group";
        } else {
            $sql="select * from ".$dbprefix."user_group where ACCESS <='" . $_SESSION['ot']["user"]["ACCESS"] . "'";
        }
        $sql = $sql . " order by 'ACCESS'";
        return getArrayFromDb($sql);

    }
    public function getUsersGroupForId($id) {
        global $dbprefix;
        $sql="select * from ".$dbprefix."user_group where ID='".$id."'";
        return getRowFromDb($sql);
    }
    public function  getNewUserGroup(){
        $userGroup=array();
        $userGroup["ID"]= 0;
        $userGroup["ACCESS"]= ACCESS_TYPE["user"];
        $userGroup["DATE_CREATE"]= date("Y-m-d H:i:s");
        $userGroup["NAME"]= "";
        $userGroup["DESCRIPTION"]= "";
        $userGroup["EMAIL"]= "";
        return $userGroup;
    }
    public function saveUserGroup($route) {
        $vars=array("NAME","ACCESS","EMAIL","DESCRIPTION");
        if ($route["ID"] == 0) {
            $userGroup=array();
            $addUpdate = ADD_DATA_TO_DATABASE;
        } else {
            $userGroup=$this->getUsersGroupForId($route["ID"],true);
            $addUpdate = UPDATE_DATA_TO_DATABASE;
        }
        foreach ($vars as $var) {
            $userGroup[$var] = $route[$var];
        }
        $sql=createSQL("user_group", $userGroup, $addUpdate, "ID", $userGroup["ID"]);
        $lid=executeSQL($sql);
    }
    public function delete($id) {
        global $dbprefix;
        $sql="delete  from ".$dbprefix."user_group where ID='".$id."'";
        executeSQL($sql);
    }

}
