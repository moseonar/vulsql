
<?php

class TicketStatus {
    public function getAllTicketStatus() {
        global $dbprefix;
        $sql="select * from ".$dbprefix."ticket_status order by `SORT`";
        return getArrayFromDb($sql);
    }
    public function getTicketStatusForId($id) {
        global $dbprefix;
        $sql="select * from ".$dbprefix."ticket_status where ID='".$id."'";
        return getRowFromDb($sql);
    }
    public function searchTicketStatusForName($search) {
        global $dbprefix;
        $sql="select * from ".$dbprefix."ticket_status where NAME like '%".$search."%'";
        return getArrayFromDb($sql);
    }
    public function getAllTicketStatusOpenClosed($closed) {
        global $dbprefix;
        $sql="select * from ".$dbprefix."ticket_status where CLOSED='".$closed."' order by `SORT`";
        return getArrayFromDb($sql);
    }

    public function save($route) {
        $vars=array("NAME","ACCESS","SORT","DESCRIPTION","COLOR");
        if ($route["ID"] == 0) {
            $ticketStatus=array();
            $addUpdate = ADD_DATA_TO_DATABASE;
        } else {
            $ticketStatus=$this->getTicketStatusForId($route["ID"]);
            $addUpdate = UPDATE_DATA_TO_DATABASE;
        }
        foreach ($vars as $var) {
            $ticketStatus[$var] = $route[$var];
        }
        $ticketStatus["CLOSED"] = (isset($route["CLOSED"])?1:0);

        $sql=createSQL("ticket_status", $ticketStatus, $addUpdate, "ID", $ticketStatus["ID"]);
        $lid=executeSQL($sql);
    }

    public function  getNewTicketStatus(){
        $ticketStatus=array();
        $ticketStatus["ID"]= 0;
        $ticketStatus["ACCESS"]= 0;
        $ticketStatus["DATE_CREATE"]= date("Y-m-d H:i:s");
        $ticketStatus["NAME"]= "";
        $ticketStatus["DESCRIPTION"]= "";
        $ticketStatus["SORT"]= 0;
        $ticketStatus["CLOSED"]= 0;
        return $ticketStatus;
    }

    public function delete($id) {
        global $dbprefix;
        $sql="delete  from ".$dbprefix."ticket_status where ID='".$id."'";
        executeSQL($sql);
    }
    public function isReady($id){
        $ticketStatus=$this->getTicketStatusForId($id);
        if($ticketStatus["CLOSED"] == 1) {
            return true;
        } else {
            return false;
        }
    }

}
